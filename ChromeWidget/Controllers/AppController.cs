﻿using Microsoft.AspNetCore.Mvc;

namespace ChromeWidget.Controllers
{
    public class AppController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IrisGDPR()
        {
            return View();
        }

        public IActionResult IrisSoundCloud()
        {
            return View();
        }
    }
}