﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChromeWidget.ViewModels
{
    public class ClientDetailsViewModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }
    }
}
