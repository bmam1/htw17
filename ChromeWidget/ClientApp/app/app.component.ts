import { Component } from '@angular/core';

@Component({
  selector: 'main-widget',
  templateUrl: "/app.component.html",
  styles: []
})
export class AppComponent {
  title = 'app';
}
